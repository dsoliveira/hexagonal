﻿using Domain.Models;
using System.Collections.Generic;

namespace Domain.Repositories
{
    public interface IUserRepository
    {
        List<User> GetUsers();
        User GetUser(string id);
        User CreateUser(User user);
    }
}
