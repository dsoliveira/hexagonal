﻿using Domain.Models;
using Domain.Repositories;
using System.Collections.Generic;

namespace Domain.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public User CreateUser(User user)
        {
            return _userRepository.CreateUser(user);
        }

        public User GetUser(string id)
        {
            return _userRepository.GetUser(id);
        }

        public List<User> GetUsers()
        {
            return _userRepository.GetUsers();
        }
    }
}
