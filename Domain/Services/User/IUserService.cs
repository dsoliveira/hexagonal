﻿using System.Collections.Generic;
using Domain.Models;

namespace Domain.Services
{
    public interface IUserService
    {
        List<User> GetUsers();
        User GetUser(string id);
        User CreateUser(User user);
    }
}
