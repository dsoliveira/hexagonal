﻿using Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Application.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;

        public UserController(ILogger<UserController> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpPost]
        [Route("user")]
        public CreateUserResponse CreateUser(CreateUserRequest createUserRequest)
        {
            var createdUser = _userService.CreateUser(createUserRequest.ToModel());
            return new CreateUserResponse(createdUser);
        }

        [HttpGet]
        [Route("user/{id}")]
        public ActionResult GetUser(string id)
        {
            var user = _userService.GetUser(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(new GetUserResponse(user));
        }

        [HttpGet]
        [Route("users")]
        public GetUsersResponse GetUsers()
        {
            return new GetUsersResponse(_userService.GetUsers());
        }
    }
}
