﻿using Domain.Models;

namespace Application.Controllers
{
    public class CreateUserRequest
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public User ToModel()
        {
            return new User()
            {
                Email = this.Email,
                Name = this.Name,
                Password = this.Password
            };
        }
    }
}
