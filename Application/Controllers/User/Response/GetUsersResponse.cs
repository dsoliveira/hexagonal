﻿using Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace Application.Controllers
{
    public class GetUsersResponse
    {
        public int Total { get; set; }

        public List<GetUserResponse> Users { get; set; }

        public GetUsersResponse(List<User> users)
        {
            this.Total = users.Count;
            this.Users = users.Select(u => new GetUserResponse(u)).ToList();
        }
    }
}
