﻿using Domain.Models;

namespace Application.Controllers
{
    public class CreateUserResponse
    {
        public string Id { get; set; }
        public string Message { get; set; }

        public CreateUserResponse(User user)
        {
            this.Id = user.Id;
            this.Message = "User created with success!";
        }
    }
}
