﻿using Domain.Models;
using Domain.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Databases
{
    public class UserRepository : IUserRepository
    {
        private readonly List<UserDao> _users = new List<UserDao>();

        public User CreateUser(User user)
        {
            user.Id = (_users.Count + 1).ToString();
            _users.Add(new UserDao(user));
            return user;
        }

        public User GetUser(string id)
        {
            return _users.FirstOrDefault(u => u.Id.Equals(id))?.ToModel();
        }

        public List<User> GetUsers()
        {
            return _users.Select(u => u.ToModel()).ToList();
        }
    }
}
